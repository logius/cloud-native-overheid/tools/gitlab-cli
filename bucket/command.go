package bucket

import (
	"github.com/spf13/cobra"
)

type flags struct {
	s3URL *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		Run: func(cmd *cobra.Command, args []string) {
			showBucketSize(cmd, &flags)
		},
		Use:   "bucket-info",
		Short: "Show S3 Bucket size",
		Long:  "This command calculates the size of the S3 buckets.",
	}

	flags.s3URL = cmd.Flags().String("url", "", "URL of S3")

	cmd.MarkFlagRequired("url")

	return cmd
}

func showBucketSize(cmd *cobra.Command, flags *flags) error {
	ShowBucketSize(*flags.s3URL)
	return nil
}
