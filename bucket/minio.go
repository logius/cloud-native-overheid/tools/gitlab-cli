package bucket

import (
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/drakkan/sftpgo/utils"
)

func ShowBucketSize(s3Url string) {
	accessKey := os.Getenv("S3_ACCESS_KEY")
	secretKey := os.Getenv("S3_SECRET_KEY")

	session := session.Must(session.NewSession())

	svc := s3.New(session, &aws.Config{
		Credentials:      credentials.NewStaticCredentials(accessKey, secretKey, ""),
		Endpoint:         aws.String(s3Url),
		Region:           aws.String("us-east-1"),
		S3ForcePathStyle: aws.Bool(true),
	})

	input := s3.ListBucketsInput{}
	buckets, err := svc.ListBuckets(&input)
	if err != nil {
		panic(err)
	}
	for _, bucket := range buckets.Buckets {
		getBucketPolicies(svc, *bucket.Name)
		getBucketSize(svc, *bucket.Name)
	}
}

func getBucketPolicies(svc *s3.S3, bucket string) {
	input := s3.GetBucketLifecycleConfigurationInput{}
	input.Bucket = &bucket
	lifecycleConfiguration, err := svc.GetBucketLifecycleConfiguration(&input)

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case "NoSuchLifecycleConfiguration":
				break
			default:
				panic(err)
			}
		}
	}
	for _, rule := range lifecycleConfiguration.Rules {
		log.Printf("Expiration Days %d", *rule.Expiration.Days)
	}
}

func getBucketSize(svc *s3.S3, bucket string) {
	params := s3.ListObjectsInput{Bucket: &bucket}

	var size int64 = 0
	err := svc.ListObjectsPages(&params,
		func(page *s3.ListObjectsOutput, lastPage bool) bool {
			for _, item := range page.Contents {
				size += *item.Size
			}

			return !lastPage
		})

	if err != nil {
		panic(err)
	}
	fmt.Printf("%30s: %s\n", bucket, utils.ByteCountIEC(size))
}
