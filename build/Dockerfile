FROM golang:1.22 AS build

WORKDIR $GOPATH/src/gitlab

# Fetch dependencies
COPY go.mod go.sum ./
RUN go mod download -x

# Copy golang code and build
COPY . . 
RUN CGO_ENABLED=0 go build -v -o /bin/gitlab-cli

# Copy to busybox 
FROM amd64/busybox:1.36.1
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=build /bin/gitlab-cli /bin/gitlab-cli

ENTRYPOINT ["/bin/gitlab-cli"]
