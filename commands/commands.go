package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/bucket"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/group"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/pipeline"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/scheduleStop"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "gitlab",
		Short: "OPS tools for GitLab",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(group.NewCommand())
	cmd.AddCommand(pipeline.NewCommand())
	cmd.AddCommand(bucket.NewCommand())
	cmd.AddCommand(scheduleStop.NewCommand())
}
