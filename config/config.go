package config

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

// GitLabGroupConfig spec
type GitLabGroupConfig struct {
	Name         string
	Roles        map[string][]string
	Rolemappings map[string][]string
}

// GitLab configuration
type GitLab struct {
	Groups []GitLabGroupConfig
}

// GroupConfig spec
type GroupConfig struct {
	Name    string   `yaml:"name"`
	Admin   bool     `yaml:"admin"`
	Members []string `yaml:"members"`
}

// Customer configuration
type Customer struct {
	Name   string
	Groups []GroupConfig
	GitLab GitLab
}

// Config contains customer configuration
type Config struct {
	Customer Customer
}

// LoadConfigfile reads config from YAML
func LoadConfigfile(fileName string) (*Config, error) {

	var config Config
	if err := readConfig(fileName, &config); err != nil {
		return nil, err
	}

	return &config, nil
}

func readConfig(fileName string, config interface{}) error {
	yamlFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		return fmt.Errorf("error reading YAML file: %s\n", err)
	}

	err = yaml.Unmarshal(yamlFile, config)
	if err != nil {
		return fmt.Errorf("error parsing YAML file: %s\n", err)
	}
	return nil
}
