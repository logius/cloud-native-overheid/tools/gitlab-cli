package gitlabclient

import (
	"fmt"
	"log"
	"os"

	"github.com/xanzy/go-gitlab"
)

// NewGitLabClient initalizes a new gitlab.Client and connects to GitLab
func NewGitLabClient(gitlabURL string) *gitlab.Client {

	gitlabAccessToken := os.Getenv("GITLAB_ACCESS_TOKEN")

	if gitlabAccessToken == "" {
		log.Fatal("Missing environment variable GITLAB_ACCESS_TOKEN")
	}

	log.Printf("Init GitLab Client %s", gitlabURL)
	git, err := gitlab.NewClient(gitlabAccessToken, gitlab.WithBaseURL(fmt.Sprintf("%s/api/v4", gitlabURL)))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	v, _, err := git.Version.GetVersion()
	if err != nil {
		log.Fatalf("%v", err)
	}
	log.Printf("GitLab version %s", v.Version)
	return git
}
