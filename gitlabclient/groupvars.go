package gitlabclient

import (
	"fmt"
	"log"

	"github.com/xanzy/go-gitlab"
)

// GroupVariable defines a GitLab group variable
type GroupVariable struct {
	Value  string `json:"value"`
	Masked bool   `json:"masked"`
}

// UpdateGroupVariable creates or updates an environment variable in a GitLab Group
func UpdateGroupVariable(gitlabClient *gitlab.Client, group *gitlab.Group, Key string, groupVar GroupVariable) error {

	groupVarExists, _, _ := gitlabClient.GroupVariables.GetVariable(group.ID, Key)

	envVarType := gitlab.EnvVariableType

	if groupVarExists == nil {
		log.Printf("Create variable %s in group %s", Key, group.FullPath)
		opt := &gitlab.CreateGroupVariableOptions{
			Key:          &Key,
			Masked:       &groupVar.Masked,
			Value:        &groupVar.Value,
			VariableType: &envVarType,
		}
		_, _, err := gitlabClient.GroupVariables.CreateVariable(group.ID, opt)
		if err != nil {
			return fmt.Errorf("failed to CreateVariable: %v", err)
		}
	} else {
		log.Printf("Update variable %s in group %s", Key, group.FullPath)
		opt := &gitlab.UpdateGroupVariableOptions{
			Masked:       &groupVar.Masked,
			Value:        &groupVar.Value,
			VariableType: &envVarType,
		}
		_, _, err := gitlabClient.GroupVariables.UpdateVariable(group.ID, Key, opt)
		if err != nil {
			return fmt.Errorf("failed to UpdateVariable: %v", err)
		}
	}
	return nil
}

// UpdateVarsInGitLabGroup updates environment vars in GitLab group
func UpdateVarsInGitLabGroup(gitlabClient *gitlab.Client, groupPath string, groupVars map[string]GroupVariable) error {

	group, err := GetGitLabGroup(gitlabClient, groupPath)
	if err != nil {
		return err
	}

	if group == nil {
		return fmt.Errorf("Could not find group %s\n", groupPath)
	}

	for key, value := range groupVars {
		if err := UpdateGroupVariable(gitlabClient, group, key, value); err != nil {
			return err
		}
	}
	return nil
}
