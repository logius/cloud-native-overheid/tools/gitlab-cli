package group

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
)

type flags struct {
	config                     *string
	gitlabURL                  *string
	dontDegradeExistingMembers *bool
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureGroups(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-group",
		Short: "Configure GitLab groups",
		Long:  "This command configures groups for tenants in GitLab.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.gitlabURL = cmd.Flags().String("gitlab_url", "", "URL of GitLab")
	flags.dontDegradeExistingMembers = cmd.Flags().Bool("dont_degrade_members", false, "Do not degrade role of existing members (default: false)")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("gitlab_url")

	return cmd
}

func configureGroups(cmd *cobra.Command, flags *flags) error {

	gitlabClient := gitlabclient.NewGitLabClient(*flags.gitlabURL)

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	log.Printf("Configure GitLab group for %q", customerConfig.Customer.Name)

	if err := ConfigureConfigFileRoleMappings(gitlabClient, customerConfig, *flags.dontDegradeExistingMembers); err != nil {
		return err
	}

	return nil
}
