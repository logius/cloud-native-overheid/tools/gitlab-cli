package group

import (
	"log"
	"strings"

	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/config"

	"github.com/xanzy/go-gitlab"
)

// ConfigureGitLabGroup configures a group in GitLab
func ConfigureGitLabGroup(gitLab *gitlab.Client, gitlabGroup config.GitLabGroupConfig) (*gitlab.Group, error) {
	log.Printf("Configure group %q", gitlabGroup)

	groups := strings.Split(gitlabGroup.Name, "/")

	var parentGroup *gitlab.Group
	var err error
	for index, groupName := range groups {
		if index == 0 {
			parentGroup, err = createRootGroup(gitLab, groupName)
			if err != nil {
				return nil, err
			}
		} else {
			parentGroup, err = createGroup(gitLab, groupName, parentGroup)
			if err != nil {
				return nil, err
			}
		}
	}
	return parentGroup, nil
}

// GitLab search does not support groupnames with 2 chars, so we cannot use the search option
func getSubGroup(gitLab *gitlab.Client, groupName string, parentGroup *gitlab.Group) (*gitlab.Group, error) {

	opt := &gitlab.ListSubGroupsOptions{
		ListOptions: gitlab.ListOptions{
			Pagination: "keyset",
			PerPage:    50,
		},
	}

	keysetPaginationParameters := []gitlab.RequestOptionFunc{}

	for {
		groups, r, err := gitLab.Groups.ListSubGroups(parentGroup.ID, opt, keysetPaginationParameters...)
		if err != nil {
			return nil, err
		}
		group := getGroup(groups, groupName)
		if group != nil {
			return group, nil
		}

		// Exit the loop when we've seen all pages.
		if r.NextLink == "" {
			break
		}

		// Set all query parameters in the next request to values in the
		keysetPaginationParameters = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(r.NextLink),
		}
	}
	return nil, nil
}

func getRootGroup(gitLab *gitlab.Client, groupName string) (*gitlab.Group, error) {
	topLevelOnly := true

	opt := &gitlab.ListGroupsOptions{
		TopLevelOnly: &topLevelOnly,
		ListOptions: gitlab.ListOptions{
			Pagination: "keyset",
			PerPage:    50,
		},
	}

	keysetPaginationParameters := []gitlab.RequestOptionFunc{}

	for {
		groups, r, err := gitLab.Groups.ListGroups(opt, keysetPaginationParameters...)
		if err != nil {
			return nil, err
		}

		group := getGroup(groups, groupName)
		if group != nil {
			return group, nil
		}

		// Exit the loop when we've seen all pages.
		if r.NextLink == "" {
			break
		}

		// Set all query parameters in the next request to values in the
		keysetPaginationParameters = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(r.NextLink),
		}
	}
	return nil, nil
}

func createGroup(gitLab *gitlab.Client, groupName string, parentGroup *gitlab.Group) (*gitlab.Group, error) {

	group, err := getSubGroup(gitLab, groupName, parentGroup)
	if err != nil {
		return nil, err
	}
	if group != nil {
		return group, nil
	}
	visibility := gitlab.PrivateVisibility
	createOpt := &gitlab.CreateGroupOptions{
		Name:       &groupName,
		Visibility: &visibility,
		Path:       &groupName,
		ParentID:   &parentGroup.ID,
	}
	log.Printf("Create child group %q", groupName)
	group, _, err = gitLab.Groups.CreateGroup(createOpt)
	if err != nil {
		return nil, err
	}
	return group, nil
}

func createRootGroup(gitLab *gitlab.Client, rootGroupName string) (*gitlab.Group, error) {
	group, err := getRootGroup(gitLab, rootGroupName)
	if err != nil {
		return nil, err
	}
	if group != nil {
		return group, nil
	}

	visibility := gitlab.PrivateVisibility
	createOpt := &gitlab.CreateGroupOptions{
		Name:       &rootGroupName,
		Visibility: &visibility,
		Path:       &rootGroupName,
	}
	log.Printf("Create root group %q", rootGroupName)
	group, _, err = gitLab.Groups.CreateGroup(createOpt)
	if err != nil {
		return nil, err
	}
	return group, nil
}

func getGroup(groups []*gitlab.Group, groupName string) *gitlab.Group {
	for _, group := range groups {
		if group.Name == groupName || group.Path == groupName {
			return group
		}
	}
	return nil
}
