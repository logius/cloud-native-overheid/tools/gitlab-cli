package group

import (
	"fmt"
	"log"

	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/config"

	"github.com/xanzy/go-gitlab"
)

var gitlabRoles = map[string]gitlab.AccessLevelValue{
	"owner":      gitlab.OwnerPermission,
	"maintainer": gitlab.MaintainerPermissions,
	"developer":  gitlab.DeveloperPermissions,
	"reporter":   gitlab.ReporterPermissions,
	"guest":      gitlab.GuestPermissions,
}

func memberExists(existingGroupMembers []*gitlab.GroupMember, userID int) bool {
	for _, member := range existingGroupMembers {
		if member.ID == userID {
			return true
		}
	}
	return false
}

func updateGitLabMember(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, roleName string, userName string, userID int) error {

	accessLevel := gitlabRoles[roleName]
	opt := gitlab.EditGroupMemberOptions{
		AccessLevel: &accessLevel,
	}

	log.Printf("Update member %q in group %q roleName %q", userName, gitlabGroup.Name, roleName)
	_, _, err := gitlabClient.GroupMembers.EditGroupMember(gitlabGroup.ID, userID, &opt)
	if err != nil {
		return err
	}
	return nil
}

func createGitLabMember(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, roleName string, userName string, userID int) (*gitlab.GroupMember, error) {

	accessLevel := gitlabRoles[roleName]
	opt := gitlab.AddGroupMemberOptions{
		UserID:      &userID,
		AccessLevel: &accessLevel,
	}

	log.Printf("Add member %q in group %q", userName, gitlabGroup.Name)
	groupMember, _, err := gitlabClient.GroupMembers.AddGroupMember(gitlabGroup.ID, &opt)
	if err != nil {
		return nil, err
	}
	return groupMember, nil
}

func getExistingGroupMember(existingGroupMembers []*gitlab.GroupMember, userID int) *gitlab.GroupMember {
	for _, member := range existingGroupMembers {
		if member.ID == userID {
			return member
		}
	}
	return nil
}

func GetGroupMembers(gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group) ([]*gitlab.GroupMember, error) {
	opt := &gitlab.ListGroupMembersOptions{
		ListOptions: gitlab.ListOptions{
			Pagination: "keyset",
			PerPage:    50,
		},
	}

	keysetPaginationParameters := []gitlab.RequestOptionFunc{}

	var allMembers = make([]*gitlab.GroupMember, 0)
	for {
		members, r, err := gitlabClient.Groups.ListGroupMembers(gitlabGroup.ID, opt, keysetPaginationParameters...)
		if err != nil {
			return nil, err
		}
		allMembers = append(allMembers, members...)

		// Exit the loop when we've seen all pages.
		if r.NextLink == "" {
			break
		}

		keysetPaginationParameters = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(r.NextLink),
		}
	}
	return allMembers, nil
}

// Search user by email or username, return first hit.
func searchGitLabUser(gitlabClient *gitlab.Client, member string) (*gitlab.User, error) {
	opt := gitlab.ListUsersOptions{
		Search: &member,
	}
	users, _, err := gitlabClient.Users.ListUsers(&opt)
	if err != nil {
		return nil, err
	}
	for _, user := range users {
		log.Printf("Found member %q [Username: %v, Email: %v, user ID: %v]", member, user.Username, user.Email, user.ID)
		if user.Email == member || user.Username == member {
			return user, nil
		}
	}
	log.Printf("NOT found member %q\n", member)
	return nil, nil
}

// Find members Customer.Groups.name[groupname].members (from config yaml)
func findConfigFileGroupMembers(customerConfig *config.Config, groupname string) []string {
	for _, group := range customerConfig.Customer.Groups {
		if groupname == group.Name {
			return group.Members
		}
	}
	return nil
}

// ConfigureConfigFileRoleMappings Configure Users by configfile rolemapping (customer.gitlab.groups.rolemapping)
func ConfigureConfigFileRoleMappings(gitlabClient *gitlab.Client, customerConfig *config.Config, dontDegradeExistingMembers bool) error {
	// for Customer.GitLab.Groups (from config yaml)
	for _, gitlabGroupConfig := range customerConfig.Customer.GitLab.Groups {
		log.Printf("ConfigureConfigFileRoleMappings for group: %q", gitlabGroupConfig)
		// Get existing group members
		gitlabGroup, err := ConfigureGitLabGroup(gitlabClient, gitlabGroupConfig)
		if err != nil {
			return err
		}
		existingGroupMembers, err := GetGroupMembers(gitlabClient, gitlabGroup)
		if err != nil {
			return err
		}
		// for Customer.GitLab.Groups.Rolemappings (from config yaml)
		for roleName, groups := range gitlabGroupConfig.Rolemappings {
			// Check if rolename is supported by Gitlab
			if gitlabRoles[roleName] < 1 {
				return fmt.Errorf("Rolename %q not supported, not in %v\n", roleName, gitlabRoles)
			}
			// for Customer.GitLab.Groups.Rolemappings.[rolename].groups (from config yaml)
			for _, groupname := range groups {
				log.Printf("ConfigureConfigFileRoleMappings roleName %q, groupname %q", roleName, groupname)
				// Find members Customer.Groups.name[].members (from config yaml)
				configFileMembers := findConfigFileGroupMembers(customerConfig, groupname)
				// for Customer.Groups.name[groupname].members (from config yaml)
				for i, member := range configFileMembers {
					log.Printf("member %d %v", i, member)
					// Lookup user in gitlab
					gitlabUser, err := searchGitLabUser(gitlabClient, member)
					if err != nil {
						return err
					}
					if gitlabUser != nil {
						if memberExists(existingGroupMembers, gitlabUser.ID) {
							existingGroupMember := getExistingGroupMember(existingGroupMembers, gitlabUser.ID)
							log.Printf("member %s, current accesslevel %+v, level in configfile: %+v (%s)", member, existingGroupMember.AccessLevel, gitlabRoles[roleName], roleName)
							if !dontDegradeExistingMembers ||
								(dontDegradeExistingMembers && gitlabRoles[roleName] > existingGroupMember.AccessLevel) {
								log.Printf("Updating member %s => role %s", member, roleName)
								err = updateGitLabMember(gitlabClient, gitlabGroup, roleName, member, gitlabUser.ID)
								if err != nil {
									return err
								}
							} else {
								log.Printf("Do not update member %s (current accesslevel Gitlab >= role in config)", existingGroupMember.Username)
							}
						} else {
							groupMember, err := createGitLabMember(gitlabClient, gitlabGroup, roleName, member, gitlabUser.ID)
							if err != nil {
								return err
							}
							existingGroupMembers = append(existingGroupMembers, groupMember)
						}
					} else {
						log.Printf("User %q does not (yet) exist in gitlab ... skip\n", member)
					}
				}
			}
		}
	}
	return nil
}
