package scheduleStop

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"
)

type flags struct {
	gitlabURL *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		Run: func(cmd *cobra.Command, args []string) {
			cleanPipelines(cmd, &flags)
		},
		Use:   "stop-schedules",
		Short: "Stop Pipeline schedules",
		Long:  "This command stops all pipeline schedules.",
	}

	flags.gitlabURL = cmd.Flags().String("gitlab_url", "", "URL of GitLab")

	cmd.MarkFlagRequired("gitlab_url")

	return cmd
}

func cleanPipelines(cmd *cobra.Command, flags *flags) error {

	gitlabClient := gitlabclient.NewGitLabClient(*flags.gitlabURL)

	scheduleStop := ScheduleStop{
		GitLabClient: gitlabClient,
	}

	scheduleStop.cleanPipelines()

	return nil
}
