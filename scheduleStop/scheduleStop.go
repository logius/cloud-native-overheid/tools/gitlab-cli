package scheduleStop

import (
	"log"

	"github.com/xanzy/go-gitlab"
)

type ScheduleStop struct {
	GitLabClient *gitlab.Client
}

// CleanupJobs deletes Jobs and artefacts older than the retention period.
func (scheduleStop ScheduleStop) cleanPipelines() {

	allProjects := scheduleStop.readProjects()

	log.Printf("Found %d projects\n", len(allProjects))
	for projectName, projectID := range allProjects {
		scheduleStop.stopSchedules(projectName, projectID)
	}
}

func (scheduleStop ScheduleStop) readProjects() map[string]int {
	opt := &gitlab.ListProjectsOptions{
		ListOptions: gitlab.ListOptions{
			Pagination: "keyset",
			PerPage:    50,
		},
	}

	keysetPaginationParameters := []gitlab.RequestOptionFunc{}

	allProjects := map[string]int{}

	for {
		gitlabProjects, r, err := scheduleStop.GitLabClient.Projects.ListProjects(opt, keysetPaginationParameters...)

		if err != nil {
			panic(err)
		}

		for _, project := range gitlabProjects {
			if !project.Archived {
				allProjects[project.PathWithNamespace] = project.ID
			}
		}

		// Exit the loop when we've seen all pages.
		if r.NextLink == "" {
			break
		}

		keysetPaginationParameters = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(r.NextLink),
		}
	}
	return allProjects
}

func (scheduleStop ScheduleStop) stopSchedules(projectName string, projectID int) {

	opt := &gitlab.ListPipelineSchedulesOptions{
		Pagination: "keyset",
		PerPage:    50,
	}

	keysetPaginationParameters := []gitlab.RequestOptionFunc{}

	for {
		schedules, r, err := scheduleStop.GitLabClient.PipelineSchedules.ListPipelineSchedules(projectID, opt, keysetPaginationParameters...)

		if err != nil {
			if r.StatusCode == 403 {
				// Oddly sometimes a 403 is raised. Probably no CICD in the project. Ignore it.
				log.Printf("Access denied reading schedules in project %s", projectName)
			} else {
				panic(err)
			}
		}

		for _, schedule := range schedules {

			if schedule.Active {
				inActive := false
				optEdit := &gitlab.EditPipelineScheduleOptions{
					Active: &inActive,
				}
				log.Printf("Disable schedule %q in project %q", schedule.Cron, projectName)
				_, _, err := scheduleStop.GitLabClient.PipelineSchedules.EditPipelineSchedule(projectID, schedule.ID, optEdit)
				if err != nil {
					log.Fatal(err)
				}
			} else {
				log.Printf("Schedule %q in project %q is not active", schedule.Cron, projectName)
			}
		}

		// Exit the loop when we've seen all pages.
		if r.NextLink == "" {
			break
		}

		keysetPaginationParameters = []gitlab.RequestOptionFunc{
			gitlab.WithKeysetPaginationParameters(r.NextLink),
		}
	}
}
